module Main where

import Criterion.Main
import MyLib

benches :: [(String, Int -> Free Maybe Int)]
benches = [ ( "rbind", rbind )
          , ( "lbind", lbind ) ]

chains :: [Int]
chains = [0..19]

-- main :: IO ()
-- main = defaultMain $
--   (\(desc, bind) clen -> bench (desc <> " " <> show clen) $ whnf (runMaybe . bind) clen)
--   <$> benches
--   <*> chains

main :: IO ()
main = defaultMain $
  [ bgroup "rbind" $ (\(desc, bind) clen -> bench (desc <> " " <> show clen)
                   $ whnf (runMaybe . bind) clen)
                     <$> [("rbind", rbind)]
                     <*> chains
  , bgroup "lbind" $ (\(desc, bind) clen -> bench (desc <> " " <> show clen)
                   $ whnf (runMaybe . bind) clen)
                     <$> [("lbind", lbind)]
                     <*> chains
  ]
  
