module MyLib (
  Free (..),
  liftF,
  lbind,
  rbind,
  runMaybe,
  someFunc) where

data Free f a =
    Pure a
  | Free (f (Free f a))

instance Functor f => Functor (Free f) where
  fmap f m = case m of
    Pure a -> Pure $ f a
    Free n -> Free $ fmap (fmap f) n 

instance Functor f => Applicative (Free f) where
  pure = Pure
  f <*> Pure m = fmap ($ m) f
  f <*> Free m = Free $ fmap (f <*>) m 

instance Functor f => Monad (Free f) where
  return = pure
  Pure a >>= f = f a
  Free m >>= f = Free $ fmap (>>= f) m

liftF :: Functor f => f a -> Free f a
liftF = Free . fmap pure

lbind :: Int -> Free Maybe Int
lbind n =
  let justf = liftF . Just
  in case n of
    0 -> justf 1
    _ -> lbind (n - 1) >> justf 1

rbind :: Int -> Free Maybe Int
rbind n =
  let justf = (liftF . Just) :: Int -> Free Maybe Int
  in case n of
    0 -> justf 1
    _ -> justf 1 >>= \_ -> rbind (n - 1)
    
runMaybe :: Free Maybe a -> Maybe a
runMaybe m = case m of
  Pure a -> Just a
  Free f -> case f of
    Nothing -> Nothing
    Just n  -> runMaybe n

someFunc :: IO ()
someFunc = putStrLn "someFunc"
